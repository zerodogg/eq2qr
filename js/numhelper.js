// @prettier
// @flow
import { initTheme } from "./theme.js";
import * as React from "react";
import ReactDOM from "react-dom";

const oneTrillion = 1000000000000;
const oneBillion = 1000000000;
const oneMillion = 1000000;

function humanReadableNumber(no, prefix, alwaysReturn) {
    if (prefix === undefined) {
        prefix = "It is";
    }
    if (typeof no !== "number") {
        no = Number.parseInt(no);
    }
    if (no / oneTrillion > 0.9) {
        return (
            prefix +
            " approximately " +
            (no / oneTrillion).toFixed(2) +
            " trillion"
        );
    } else if (no / oneBillion > 0.9) {
        return (
            prefix +
            " approximately " +
            (no / oneBillion).toFixed(2) +
            " billion"
        );
    } else if (no / oneMillion > 0.9) {
        return (
            prefix +
            " approximately " +
            (no / oneMillion).toFixed(2) +
            " million"
        );
    }
    if (alwaysReturn === true) {
        return prefix + " " + no;
    }
    return "";
}

type NumberEntryProps = {|
    onChange: (string) => void,
    value: string | number,
|};
function NumberEntry(props: NumberEntryProps) {
    const { onChange, value } = props;
    const setNumber = (ev) => {
        let number = ev.target.value;
        if (number !== "") {
            number = Number.parseInt(number).toString();
        }
        onChange(number);
    };
    return (
        <form
            autoComplete="off"
            onSubmit={(e) => {
                e.preventDefault();
            }}
        >
            <input
                type="number"
                placeholder="Enter number"
                value={value}
                name="query"
                required
                onChange={setNumber}
            />
        </form>
    );
}

type NumberInfoProps = {|
    value: null | string,
|};
function NumberInfo(props: NumberInfoProps) {
    const { value } = props;
    if (value === null || value === undefined || value === "") {
        return <div />;
    }
    const humanReadable = humanReadableNumber(value);
    return (
        <div className="mt-05">
            <div>
                {Number.parseInt(value).toLocaleString()} is {value.length}{" "}
                digits.
            </div>
            <div>{humanReadable !== "" ? humanReadable + "." : ""}</div>
        </div>
    );
}

type NumberComparisonProps = {|
    no1: null | string,
    no2: null | string,
|};
function NumberComparison(props: NumberComparisonProps) {
    const { no1, no2 } = props;
    if (
        no1 === null ||
        no1 === undefined ||
        no1 === "" ||
        no2 === null ||
        no2 === undefined ||
        no2 === ""
    ) {
        return <div />;
    }
    const no1Int = Number.parseInt(no1);
    const no2Int = Number.parseInt(no2);
    let largest, difference;
    let timesHigher = null;
    if (no1Int === no2Int) {
        largest = "The numbers are identical";
    } else if (no1Int > no2Int) {
        largest =
            "The first number is the largest (" +
            no1Int.toLocaleString() +
            ").";
        difference = no1Int - no2Int;
        timesHigher = (no1Int / no2Int).toFixed(2);
    } else {
        largest =
            "The second number is the largest (" +
            no2Int.toLocaleString() +
            ").";
        difference = no2Int - no1Int;
        timesHigher = (no2Int / no1Int).toFixed(2);
    }
    if (
        timesHigher !== null &&
        (Number.parseFloat(timesHigher) <= 1.0 ||
            Number.parseFloat(timesHigher) >= 10000)
    ) {
        timesHigher = null;
    }
    return (
        <div className="mt-1">
            <h4>Comparison</h4>
            <div>{largest}</div>
            {difference === undefined
                ? ""
                : humanReadableNumber(difference, "It is", true) +
                  (timesHigher !== null ? " (" + timesHigher + " times)" : "") +
                  " larger."}
        </div>
    );
}

type NumHelperAppState = {|
    numberOne: string,
    numberTwo: string,
|};
class NumHelperApp extends React.Component<{||}, NumHelperAppState> {
    constructor() {
        super();
        this.state = {
            numberOne: "",
            numberTwo: "",
        };
    }
    render() {
        return (
            <div>
                <div className="flex-container no-flex-on-mobile">
                    <div className="flex-fixed-item">
                        <h5 className="onlyMobile">First number</h5>
                        <NumberEntry
                            value={this.state.numberOne}
                            onChange={(no) => this.setState({ numberOne: no })}
                        />
                        <NumberInfo value={this.state.numberOne} />
                    </div>
                    <div className="flex-fixed-item">
                        <h5 className="onlyMobile">Second number</h5>
                        <NumberEntry
                            value={this.state.numberTwo}
                            onChange={(no) => this.setState({ numberTwo: no })}
                        />
                        <NumberInfo value={this.state.numberTwo} />
                    </div>
                </div>
                <NumberComparison
                    no1={this.state.numberOne}
                    no2={this.state.numberTwo}
                />
            </div>
        );
    }
}

function initialize() {
    initTheme();
    const root = document.getElementById("root");
    if (root) {
        ReactDOM.render(<NumHelperApp />, root);
    }
}
initialize();
