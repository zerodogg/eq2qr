// @flow
import { DateTime } from "luxon";
import { initTheme, configureTheme } from "./theme.js";
import "regenerator-runtime/runtime";

const DOMHelpers = {
    textFromElement(element: HTMLElement | Element | Node): string {
        if (element.innerText !== undefined && element.innerText !== null) {
            return element.innerText;
        }
        return "";
    },
    getElementById(query: string): HTMLElement {
        const element = document.getElementById(query);
        if (element === null || element === undefined) {
            throw "Unable to getElementById(" + query + ")";
        }
        return element;
    },

    getBody(): HTMLBodyElement {
        const body = document.body;
        if (body === null) {
            throw "Unable to find body";
        }
        return body;
    },

    getOffsetFromTopForElement(element: HTMLElement): number {
        const Y = element.getBoundingClientRect().top;
        return Y + window.scrollY;
    },
};

const menuHandlers = {
    listen() {
        const menuItems = document.querySelectorAll("menu > a");
        for (const menu of menuItems) {
            menu.addEventListener("click", (e: Event) => {
                e.preventDefault();
                const href = menu.getAttribute("href");
                if (typeof href !== "string") {
                    throw "Missing href?";
                }
                menuHandlers.scrollToItem(href);
            });
        }
    },
    scrollToItem(href: string) {
        const hash = href.substr(1);
        const element = DOMHelpers.getElementById(hash);
        const scrollTarget =
            DOMHelpers.getOffsetFromTopForElement(element) - 70;
        window.scrollTo({
            top: scrollTarget,
            left: 0,
            behavior: "smooth",
        });
        history.pushState({}, "", href);
    },
};

const wikiSearcher: {
    _prevQuery: string,
    _abortController: null | AbortController,
    _searchRequestTimeout: null | TimeoutID,
    _safeSearchTimeout: null | TimeoutID,
    abortRunning: () => void,
    safeSearcher: (string) => void,
    nowSearching: () => void,
    nowShowingResults: () => void,
    clearResults: () => void,
    hide: () => void,
    clearRequestTimeout: () => void,
    noResultsFound: (string) => void,
    search: (
        query: string,
        force: ?boolean,
        attemptNumber: ?number,
    ) => Promise<void>,
    _resultsElement: () => Element,
    _nowSearchingElement: () => Element,
    __element_eq2wikiNowSearching: null | Element,
    __element_eq2wikiResults: null | Element,
} = {
    _prevQuery: "",
    _abortController: null,
    _searchRequestTimeout: null,
    _safeSearchTimeout: null,
    __element_eq2wikiNowSearching: null,
    __element_eq2wikiResults: null,

    safeSearcher(query) {
        if (query === wikiSearcher._prevQuery) {
            return;
        }
        wikiSearcher.nowSearching();
        if (wikiSearcher._searchRequestTimeout !== null) {
            clearTimeout(wikiSearcher._searchRequestTimeout);
        }
        wikiSearcher._searchRequestTimeout = setTimeout(() => {
            wikiSearcher.search(query);
        }, 500);
    },

    async search(query, force, attemptNumber) {
        try {
            wikiSearcher.abortRunning();
            if (query === wikiSearcher._prevQuery && force !== true) {
                return;
            }
            wikiSearcher._prevQuery = query;
            const abortController = new AbortController();
            wikiSearcher._abortController = abortController;
            wikiSearcher.nowSearching();
            // Empty old results
            wikiSearcher.clearResults();

            wikiSearcher.clearRequestTimeout();
            wikiSearcher._searchRequestTimeout = setTimeout(() => {
                let attempt = attemptNumber;
                if (attempt === null || attempt === undefined) {
                    attempt = 0;
                }
                if (attempt >= 4) {
                    wikiSearcher.abortRunning();
                    return wikiSearcher.noResultsFound(query);
                }
                wikiSearcher.search(query, true, ++attempt);
            }, 3500);
            const jsonResult = await fetch(
                "https://eq2.fandom.com/api.php?action=opensearch&format=json&origin=*&search=" +
                    encodeURIComponent(query),
                {
                    signal: abortController.signal,
                },
            );
            wikiSearcher.clearRequestTimeout();
            const result = await jsonResult.json();
            const titles = result[1];
            const URLs = result[3];

            const resultContainer = wikiSearcher._resultsElement();

            const header = document.createElement("h4");
            header.innerText = "Results from the EQ2i wiki";
            resultContainer.appendChild(header);

            for (let i = 0; i < titles.length; i++) {
                const container = document.createElement("div");
                container.classList.add("mt-03");
                const a = document.createElement("a");
                a.setAttribute("href", URLs[i]);
                a.innerText = titles[i];
                container.appendChild(a);
                resultContainer.appendChild(container);
            }
            if (titles.length === 0) {
                return wikiSearcher.noResultsFound(query);
            } else {
                const moreResultsContainer = document.createElement("div");
                moreResultsContainer.classList.add("mt-1");
                const a = document.createElement("a");
                a.setAttribute(
                    "href",
                    "https://eq2.fandom.com/wiki/Special:Search?query=" +
                        encodeURIComponent(query),
                );
                a.innerText = "View more results on eq2.fandom.com";
                moreResultsContainer.appendChild(a);
                resultContainer.appendChild(moreResultsContainer);
            }
            wikiSearcher.nowShowingResults();
        } catch (_) {} // eslint-disable-line
    },
    noResultsFound(query) {
        const noResults = document.createElement("div");
        noResults.innerText =
            'Found no results for "' + query + '" on the wiki';
        wikiSearcher._resultsElement().appendChild(noResults);
        wikiSearcher.nowShowingResults();
    },
    hide() {
        wikiSearcher._nowSearchingElement().classList.add("d-none");
        wikiSearcher._resultsElement().classList.add("d-none");
        wikiSearcher.abortRunning();
    },
    nowSearching() {
        wikiSearcher._nowSearchingElement().classList.remove("d-none");
        wikiSearcher._resultsElement().classList.add("d-none");
    },
    nowShowingResults() {
        wikiSearcher._nowSearchingElement().classList.add("d-none");
        wikiSearcher._resultsElement().classList.remove("d-none");
    },
    clearResults() {
        const from = wikiSearcher._resultsElement();
        while (from.firstChild) {
            from.removeChild(from.firstChild);
        }
    },
    clearRequestTimeout() {
        if (wikiSearcher._searchRequestTimeout !== null) {
            clearTimeout(wikiSearcher._searchRequestTimeout);
        }
    },
    abortRunning() {
        if (wikiSearcher._abortController !== null) {
            try {
                wikiSearcher._abortController.abort();
            } catch (_) {} // eslint-disable-line
        }
    },
    _nowSearchingElement() {
        if (wikiSearcher.__element_eq2wikiNowSearching === null) {
            wikiSearcher.__element_eq2wikiNowSearching =
                DOMHelpers.getElementById("eq2wikiNowSearching");
            if (wikiSearcher.__element_eq2wikiNowSearching === null) {
                throw "Did not find dom element for eq2wikiNowSearching";
            }
        }
        return wikiSearcher.__element_eq2wikiNowSearching;
    },
    _resultsElement() {
        if (wikiSearcher.__element_eq2wikiResults === null) {
            wikiSearcher.__element_eq2wikiResults =
                DOMHelpers.getElementById("eq2wikiResults");
            if (wikiSearcher.__element_eq2wikiResults === null) {
                throw "Did not find dom element for eq2wikiResults";
            }
        }
        return wikiSearcher.__element_eq2wikiResults;
    },
};

const pageSearcher: {
    filterTablesBy: (string) => void,
    initialize: () => void,
    setSearchValue: (string) => void,
    toggleSearchValue: (string) => boolean,
    _previousFilter: string,
} = {
    _previousFilter: "",
    // Filters the page by the text string supplied
    filterTablesBy(filter) {
        if (filter === pageSearcher._previousFilter) {
            return;
        }
        pageSearcher._previousFilter = filter;
        var quickSearch = DOMHelpers.getElementById("eq2QuickSearch");
        if (quickSearch === null) {
            throw "Failed to find eq2QuickSearch element";
        }
        var tables = document.querySelectorAll("table");
        var queries = filter.toLowerCase().split(/\s+/);
        let specialQuery = false;
        if (filter === "expansions:only-level-increases") {
            queries = ["increased the level cap"];
            specialQuery = true;
        }

        const body = DOMHelpers.getBody();
        body.setAttribute("data-filter", filter);

        let sectionsVisible = false;
        for (var table of tables) {
            var visibleRows = 0;
            var parentNode = table.parentNode;
            // Make flow happy
            if (!(parentNode instanceof HTMLElement)) {
                throw "Impossible";
            }
            var parentHeader = DOMHelpers.textFromElement(
                parentNode.querySelector("h1"),
            ).toLowerCase();
            var rows = table.querySelectorAll("tr");
            for (var row of rows) {
                if (row.querySelectorAll("th").length > 0) {
                    continue;
                }
                if (filter == "") {
                    row.style.display = "table-row";
                    visibleRows++;
                } else {
                    if (row.classList.contains("duplicate")) {
                        row.style.display = "none";
                        continue;
                    }
                    var matches = 0;
                    var rowText = DOMHelpers.textFromElement(row).toLowerCase();
                    for (var query of queries) {
                        if (
                            rowText.indexOf(query) != -1 ||
                            parentHeader.indexOf(query) != -1
                        ) {
                            matches++;
                        }
                    }
                    if (matches == queries.length) {
                        row.style.display = "table-row";
                        visibleRows++;
                    } else {
                        row.style.display = "none";
                    }
                }
            }
            if (visibleRows == 0) {
                parentNode.style.display = "none";
            } else {
                parentNode.style.display = "block";
                sectionsVisible = true;
            }
        }
        if (filter === "" || specialQuery === true) {
            // No filters means no search link to eq2i
            quickSearch.classList.remove("visible");
            wikiSearcher.hide();
        } else {
            var notWhatYouWereLookingFor = DOMHelpers.getElementById(
                "notWhatYouWereLookingFor",
            );
            var noHits = DOMHelpers.getElementById("noHits");
            // We have a search string. If we found results, we show the "not what you were looking for?" text.
            // Otherwise we show the "No results found" text.
            DOMHelpers.getElementById("quickSearchString").innerText = filter;
            DOMHelpers.getElementById("quickSearchLink").setAttribute(
                "href",
                "https://eq2.fandom.com/wiki/Special:Search?query=" +
                    encodeURIComponent(filter),
            );
            if (sectionsVisible === true) {
                noHits.classList.remove("visible");
                notWhatYouWereLookingFor.classList.add("visible");
                quickSearch.classList.add("smaller");
                wikiSearcher.hide();
            } else {
                noHits.classList.add("visible");
                notWhatYouWereLookingFor.classList.remove("visible");
                quickSearch.classList.remove("smaller");
                wikiSearcher.safeSearcher(filter);
            }
            quickSearch.classList.add("visible");
        }
    },
    initialize() {
        var searchElement = DOMHelpers.getElementById("search");
        DOMHelpers.getElementById("searchForm").addEventListener(
            "reset",
            () => {
                pageSearcher.filterTablesBy("");
            },
        );
        if (!(searchElement instanceof HTMLInputElement)) {
            throw "searchElement isn't an HTMLInputElement. Confused.";
        }
        searchElement.addEventListener("keyup", () => {
            pageSearcher.filterTablesBy(searchElement.value);
        });

        for (const entry of document.querySelectorAll("abbrev")) {
            entry.addEventListener("click", () => {
                const searchKey = entry.getAttribute("data-key");
                if (searchKey !== null && searchKey !== undefined) {
                    pageSearcher.setSearchValue(searchKey);
                    window.scrollTo({ top: 0, left: 0, behaviour: "smooth" });
                    history.pushState({}, "", "#");
                }
            });
        }
    },

    setSearchValue(str: string) {
        const searchElement = DOMHelpers.getElementById("search");
        if (!(searchElement instanceof HTMLInputElement)) {
            throw "searchElement isn't an HTMLInputElement. Confused.";
        }
        searchElement.value = str;
        pageSearcher.filterTablesBy(str);
    },

    toggleSearchValue(str: string): boolean {
        if (pageSearcher._previousFilter === str) {
            pageSearcher.setSearchValue("");
            return false;
        } else {
            pageSearcher.setSearchValue(str);
            return true;
        }
    },
};
function initializeTimezoneConversion() {
    const PSTat11PM = DateTime.fromObject(
        { hour: 23, weekday: 4 },
        { zone: "America/Los_Angeles" },
    );
    const inLocalTime = PSTat11PM.setZone(
        Intl.DateTimeFormat().resolvedOptions().timeZone,
    );
    const pstToLocalTimeElements =
        document.querySelectorAll(".pst-to-local-time");
    for (const pstToLocalTimeElement of pstToLocalTimeElements) {
        pstToLocalTimeElement.innerText =
            inLocalTime.toLocaleString(DateTime.TIME_SIMPLE) + " ";
        const parent = pstToLocalTimeElement.parentNode;
        if (parent instanceof HTMLElement) {
            parent.classList.remove("hidden");
        }
    }

    const thursdayPSTElements = document.querySelectorAll(
        ".pst-thursday-to-local",
    );
    for (const thursdayPST of thursdayPSTElements) {
        // Needs an extra space to not cling to the next word
        thursdayPST.innerText = PSTat11PM.setLocale("en").weekdayLong + " ";
    }

    updatePST();
    setInterval(updatePST, 20000);
}

function updatePST() {
    const now = DateTime.local().setZone("America/Los_Angeles");
    const nextDailyResetTime = DateTime.fromObject(
        { hour: 23 },
        { zone: "America/Los_Angeles" },
    ).toRelative({ locale: "en" });

    let nextThursday = now;
    if (now.weekday === 4 && now.hour >= 11) {
        nextThursday = nextThursday.plus({ days: 1 });
    }
    while (nextThursday.weekday !== 4) {
        nextThursday = nextThursday.plus({ days: 1 });
    }
    nextThursday = nextThursday.set({ hours: 11, minutes: 0 });
    const nextWeeklyResetTime = nextThursday.toRelative({
        locale: "en",
    });

    const pstToLocalTimeElements =
        document.querySelectorAll(".pst-to-local-time");
    for (const pstToLocalTimeElement of pstToLocalTimeElements) {
        pstToLocalTimeElement.setAttribute(
            "title",
            "The time is now " +
                now.setLocale("en").toLocaleString(DateTime.TIME_SIMPLE) +
                " PST",
        );
    }

    const weeklyCounterElements = document.querySelectorAll(
        ".next-weekly-reset-counter",
    );
    for (const counter of weeklyCounterElements) {
        counter.innerText =
            " The next weekly reset is in " + nextWeeklyResetTime + ". ";
    }

    const dailyCounterElements = document.querySelectorAll(
        ".next-daily-reset-counter",
    );
    for (const counter of dailyCounterElements) {
        counter.innerText =
            " The next daily reset is in " + nextDailyResetTime + ". ";
    }
}

function enableReturnToTop() {
    // First set up an IntersectionObserver that adds the "is-sticky" class to
    // the searchSection element. The "return to top" icon is only displayed
    // when the search bar is stickied.
    const el = DOMHelpers.getElementById("searchSection");
    const observer = new IntersectionObserver(
        ([e]) =>
            e.target.classList.toggle("is-sticky", e.intersectionRatio < 1),
        { threshold: [1] },
    );
    observer.observe(el);
    // Handle clicks of the return to top icon.
    DOMHelpers.getElementById("returnToTop").addEventListener("click", () => {
        window.scrollTo({
            top: 0,
            left: 0,
            behavior: "smooth",
        });
        history.pushState({}, "", "#");
    });
}

function initializeClickHandlers() {
    menuHandlers.listen();
    DOMHelpers.getElementById("expacShowOnlyLevelInc").addEventListener(
        "click",
        (e: Event) => {
            e.preventDefault();
            if (
                pageSearcher.toggleSearchValue(
                    "expansions:only-level-increases",
                ) === true
            ) {
                window.scrollTo({ top: 0, left: 0, behaviour: "smooth" });
            } else {
                menuHandlers.scrollToItem("#expansions");
            }
        },
    );
}

function main() {
    // $FlowIgnore[incompatible-use]
    document.body.classList.add("has-js");
    window.setTheme = (theme) => {
        localStorage.setItem("theme", theme);
        configureTheme(theme);
    };

    initTheme();
    pageSearcher.initialize();
    initializeTimezoneConversion();
    // Autofocus on desktops
    if (navigator.userAgent.toLowerCase().indexOf("mobile") == -1) {
        document.querySelector('input[type="search"]')?.focus();
    }

    enableReturnToTop();
    initializeClickHandlers();
}
main();
