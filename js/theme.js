function configureTheme(theme) {
    if (theme !== "light") {
        // $FlowIgnore[incompatible-use]
        document.body.classList.remove("theme-light");
    } else {
        // $FlowIgnore[incompatible-use]
        document.body.classList.add("theme-light");
    }
}

function initTheme() {
    const theme = localStorage.getItem("theme");
    if (theme !== undefined && theme !== null && theme === "light") {
        configureTheme(theme);
    }
}
export { configureTheme, initTheme };
