#!/usr/bin/perl
package EQ2QR;
use base 'Exporter';
use v5.20;
use feature qw(signatures);
no warnings "experimental::signatures";
use Carp;
use IO::All;
use Mojo::DOM;
use JSON;
use YAML                  qw(LoadFile);
use File::Basename        qw( dirname );
use File::Spec::Functions qw( rel2abs );

sub sortableEntry ($entry, $source = undef)
{
    if (   defined $source
        && ref($source->{$entry}) eq 'HASH'
        && defined $source->{$entry}->{sortKey})
    {
        return $source->{$entry}->{sortKey};
    }
    $entry =~ s/<[^>]+>//g;
    return $entry;
}

sub getPathTo
{
    my $file = shift;
    state $directory = dirname(rel2abs(__FILE__));
    foreach my $attempt (qw(/ /../ /../parts/))
    {
        if (-e $directory . $attempt . $file)
        {
            return $directory . $attempt . $file;
        }
    }
    die("Failed to locate $file");
}

sub getContentFromKey ($key, $source)
{
    if (ref $source->{$key})
    {
        return $source->{$key}->{content};
    }
    return $source->{$key};
}

sub loadFrom
{
    state %loaded;
    my $file = shift;
    if (!defined $loaded{$file})
    {
        if ($file =~ /\.ya?ml$/)
        {
            $loaded{$file} =
              LoadFile(getPathTo($file));
        } else
        {
            $loaded{$file} =
              decode_json(io->file(getPathTo($file))->slurp);
        }
    }
    return $loaded{$file};
}

sub loadFromJSON
{
    return loadFrom(@_);
}

sub entryFromAny
{
    my $entry = shift;
    foreach my $href (@_)
    {
        if (defined $href && defined $href->{$entry})
        {
            return ($entry, getContentFromKey($entry, $href));
        }
    }
    # Failed, try fuzzy searching
    foreach my $href (@_)
    {
        next if !defined($href);
        foreach my $key (
            sort {
                length(sortableEntry($a, $href)) <=>
                  length(sortableEntry($b, $href))
            }
            keys %{$href}
          )
        {
            if (index(lc(sortableEntry($key)), lc($entry)) != -1)
            {
                return ($key, getContentFromKey($key, $href));
            }
        }
    }
    return;
}

sub autoEntryFromAny ($string, $source)
{
    my ($result, $content) = entryFromAny($string, $source);
    if (defined $result)
    {
        return ($result, $content, 0);
    }
    my ($result, $content) = entryFromAny($string, loadAllJSON());
    if (defined $result)
    {
        return ($result, $content, 1);
    }
    return;
}

sub abbrevFromHTML ($html)
{
    my $text = Mojo::DOM->new($html)->all_text;
    # Hack to filter expansion text
    $text =~ s{\s*Questlines:\s*(adventure)?,?\s*(tradeskill)?\s*\.$}{};
    return $text;
}

sub resolveAbbrevs
{
    my $string = shift;
    croak("string supplied to resolveAbbrevs is undef") if !defined $string;
    my $dom = Mojo::DOM->new($string);
    foreach my $abbrev ($dom->find('abbrev')->each)
    {
        my $key = $abbrev->attr('title');
        $key //= $abbrev->all_text;
        my ($found, $content) = entryFromAny($key, @_);
        if (defined $abbrev->attr('prefix-content-with-key'))
        {
            delete $abbrev->attr->{'prefix-content-with-key'};
            $content = $found . ': ' . $content;
        }
        if (!defined $content)
        {
            die("Unable to resolve abbrev for '" . $key . '"');
        }
        $abbrev->attr('data-key', Mojo::DOM->new($found)->all_text);
        $abbrev->attr('title',    abbrevFromHTML($content));
    }
    return $dom->to_string;
}

sub autoResolveAbbrevs
{
    my $string = shift;
    return resolveAbbrevs($string, @_, loadAllJSON());
}

sub loadAllJSON
{
    return (
        loadFromJSON('terminology.json'),
        loadFromJSON('terminology-tradeskills.json'),
        loadFromJSON('addons.json'),
        loadFromJSON('returner.json'),
        loadFromJSON('expansions.json'),
    );
}

sub sectionHeader ($id, $title, $terminology, $description,
    $tableDescription = undef)
{
    my $tableDescriptionOut = "";
    if (defined $tableDescription)
    {
        $tableDescriptionOut =
          '<div class="table-description">' . $tableDescription . '</div>';
    }
    my $out = '<section id="' . $id . '">
		<h1>' . $title . '</h1>' . $tableDescriptionOut . '
		<table>';
    if ($terminology && $description)
    {
        $out .= '<thead>
				<tr>
					<th>' . $terminology . '</th>
					<th>' . $description . '</th>
				</tr>
			</thead>';
    }
    $out .= '<tbody>';
}

sub sectionFooter ()
{
    return '</tbody></table>
		</section>';
}

our @EXPORT =
  qw(sortableEntry loadFromJSON entryFromAny resolveAbbrevs autoResolveAbbrevs loadFrom sectionHeader sectionFooter getContentFromKey autoEntryFromAny);

1;
