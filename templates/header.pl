#!/usr/bin/perl
use v5.020;
use Getopt::Long;
my $description =
"A curated reference containing useful resources for new, current and returning EverQuest II players";
my $subtitle;
my $isMainPage;
GetOptions(
    'subtitle=s'    => \$subtitle,
    'description=s' => \$description,
    'is-main-page'  => \$isMainPage,
) || die;
my $title = 'EverQuest II: A reference';
if ( defined $subtitle ) {
    $title = $subtitle . ' - ' . $title;
}
say '<!DOCTYPE html>
<html>
    <head>
        <title>' . $title . '</title>
        <meta property="og:title" content="' . $title . '" />
        <meta name="description" content="' . $description . '" />
        <meta property="og:description" content="' . $description . '" />
        <meta property="og:type" content="website" />
        <meta charset=utf-8 />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="/style.css?rev={{{STYLE_GIT_REVISION}}}" />
        <link rel="shortcut icon" href="/icon.png" />
        <noscript>
            <style type="text/css">#searchForm { display: none; }</style>
        </noscript>
    </head>
    <body>
        <script>0/* Work around flash-of-unstyled-content in Firefox: https://bugzilla.mozilla.org/show_bug.cgi?id=1404468 */</script><main>';

if ($isMainPage) {
    say '<header>
                <h1>EverQuest II: A reference</h1>
            </header>
            <section>
                This is a curated reference site for new, current and returning
                players of <a href="https://www.everquest2.com/" class="assassin-link">EverQuest II</a>.
            </section>
            <menu>
                <a href="#websites">Websites</a>
                <a href="#tools">Tools</a>
                <a href="#other">Useful resources</a>
                <a href="#terminology">Terminology</a>
                <a href="#tradeterms">Tradeskill terminology</a>
                <a href="#addons">Addons</a>
                <a href="#returner">Returning player resources</a>
                <a href="#youtubers">YouTubers and content creators</a>
                <a href="#liveevents">Live events</a>
                <a href="#expansions">Expansions</a>
            </menu>
            <section id="themeToggle">
            <div class="requires-js">
            Theme:
            <a href="#" class="darkToggleButton" onclick="event.preventDefault(); setTheme(\'dark\');">dark</a>
            <a href="#" class="lightToggleButton" onclick="event.preventDefault(); setTheme(\'light\');">light</a>
            </div>
            </section>
            <section id="searchSection">
                <form id="searchForm" autocomplete="off">'
      .

      # Cross-browser form submission disabling without js by having a hidden
      # button that is disabled, since form submission is actually pressing the
      # first button in the form
      '
                    <button type="submit" disabled class="d-none" aria-hidden="true"></button>
                    <input id="search" type="search" placeholder="Search" value="" name="query" required title="Searches this website as you type. If no results are found then it will automatically search the EQ2i wiki and display the results." />
                    <input type="reset" value="❌" />
                </form>
                <div id="returnToTop">
                    <span class="icon-angle-circled-up" title="Return to top" />
                </div>
            </section>';
}
