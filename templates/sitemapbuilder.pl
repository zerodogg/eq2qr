#!/usr/bin/perl
use v5.020;
use Fatal qw(chdir);
use File::Find qw(find);
if (!-e "./public/index.html")
{
    die "You must build the main site before building the sitemap";
}
chdir("./public");
my @files;
find({
        wanted => sub {
            if (/\.html$/)
            {
                s/^\.//;
                s/index\.html$//g;
                push(@files, $_);
            }
        },
        no_chdir => 1,
    },
    "./"
);

my %changefreq = ('/number-helper/' => 'monthly',);

say '<?xml version="1.0" encoding="UTF-8"?>';
print '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach my $url (@files)
{
    my $urlChangefreq = $changefreq{$url} // 'weekly';
    print '<url><loc>https://eq2reference.org'
      . $url
      . '</loc><changefreq>'
      . $urlChangefreq
      . '</changefreq></url>';
}
print '</urlset>';
