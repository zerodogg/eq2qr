#!/usr/bin/perl
use v5.20;
use Time::Moment;
use FindBin;
use Getopt::Long;
use lib "$FindBin::Bin/../lib";
use EQ2QR;

my @order;
my @orderEnd;
my $renderFile;
my $headerID;
my $headerName;
my $tableDescription;

GetOptions(
    'render-file=s'       => \$renderFile,
    'header-id=s'         => \$headerID,
    'header-name=s'       => \$headerName,
    'table-description=s' => \$tableDescription,
) || die;

die("--render-file is required") if !defined($renderFile);
die("--header-id is required")   if !defined($headerID);
die("--header-name is required") if !defined($headerName);

my %terms = %{ loadFromJSON($renderFile) };

sub populateWithCityFestivalsAndMoonlight {
    my $finalEventDate = momentFromString( getFinalEvent() )->plus_months(1);
    my @festivals      = (
        undef,
        "Gorowyn city festival",
        "Kelethin city festival",
        "Neriak city festival",
        "Qeynos city festival",
        "Freeport city festival",
        "New Halas city festival",
        "Gorowyn city festival",
        "Kelethin city festival",
        "Neriak city festival",
        "Qeynos city festival",
        "Freeport city festival",
        "New Halas city festival",
    );
    my $now = now();
    my $max = 99;
    for ( my $current = 0 ; $current < $max ; $current++ ) {
        my $year  = $now->year;
        my $month = sprintf( "%02d", $now->month );
        last if $now > $finalEventDate;
        my $festival = $festivals[$month];
        my $id       = $year . '-' . $month . '-01' . $festival;
        my $linkName = $festival;
        $linkName =~ s/^(\S+)\s+.*/$1/;
        $terms{$id} = {
            title   => $festival,
            start   => $year . '-' . $month . '-01',
            end     => $year . '-' . $month . '-08',
            link    => 'https://eq2.fandom.com/wiki/City_Festival#' . $linkName,
            regular => 1,
        };
        $id = $year . '-' . $month . '-20';
        $terms{$id} = {
            title   => 'Moonlight enchantments',
            start   => $year . '-' . $month . '-20',
            end     => $year . '-' . $month . '-22',
            link    => 'https://eq2.fandom.com/wiki/Moonlight_Enchantments',
            regular => 1,
        };
        $now = $now->plus_months(1);
    }
}

sub getFinalEvent() {
    my $eventKey = ( reverse sort keys %terms )[0];
    my $event    = %terms{$eventKey};
    return $event->{end};
}

populateWithCityFestivalsAndMoonlight();

say sectionHeader( $headerID, $headerName, undef, undef, $tableDescription );

sub momentFromString {
    my $dateString = shift;
    my ( $y, $m, $d ) = split( '-', $dateString );
    return Time::Moment->new(
        year  => $y,
        month => $m,
        day   => $d
    );
}

sub now {
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) =
      localtime(time);
    $year += 1900;
    $mon++;
    return Time::Moment->new(
        year  => $year,
        month => $mon,
        day   => $mday
    );

}

sub hasPassed {
    my $dateString = shift;
    my $date       = momentFromString($dateString);
    my $now        = now();
    return $now > $date;
}

my $humanDateFormat = '%d. %b';

my $currentDate = now();

my %monthHeaders;
foreach my $lookupTerm ( sort keys %terms ) {
    my $start   = %terms{$lookupTerm}->{start};
    my $end     = %terms{$lookupTerm}->{end};
    my $title   = %terms{$lookupTerm}->{title};
    my $link    = %terms{$lookupTerm}->{link};
    my $regular = %terms{$lookupTerm}->{regular};
    next if hasPassed($end);
    my $endMoment   = momentFromString($end);
    my $startMoment = momentFromString($start);
    my $monthID     = $startMoment->strftime("%B %Y");

    if ( $startMoment > $endMoment ) {
        die("$lookupTerm: ends before it starts");
    }

    if (   $startMoment < $currentDate
        && $currentDate->month != $startMoment->month )
    {
        $monthID = $currentDate->strftime("%B %Y");
    }

    if ( !$monthHeaders{$monthID} ) {
        say "<tr class='duplicate'><td colspan='2' class='text-center'><b>"
          . $monthID
          . "</b></td></tr>";
        $monthHeaders{$monthID} = 1;
    }
    my $fromTo = '';
    if ( $startMoment->month == $endMoment->month ) {
        $fromTo = $startMoment->strftime("%d") . ' - ';
        $fromTo .= $endMoment->strftime("%d %b");
    }
    elsif ( $startMoment->year != $endMoment->year ) {
        $fromTo = $startMoment->strftime("%d %b %Y") . ' - ';
        $fromTo .= $endMoment->strftime("%d %b %Y");
    }
    else {
        $fromTo = $startMoment->strftime("%d %b") . ' - ';
        $fromTo .= $endMoment->strftime("%d %b");
    }
    if ( !$link ) {
        $link = 'https://eq2.fandom.com/wiki/' . $title;
        $link =~ s/\s+celebration//;
        $link =~ s/\s+/_/g;
    }
    my $titleWithLink = '<a href="' . $link . '">' . $title . '</a>';
    if ( !$regular ) {
        $titleWithLink = '<b>' . $titleWithLink . '</b>';
    }
    my $class = "";
    say '<tr'
      . $class . '><td>'
      . $titleWithLink
      . '</td><td>'
      . $fromTo
      . '.</td></tr>';
}
say sectionFooter();
