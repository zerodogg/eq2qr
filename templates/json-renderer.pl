#!/usr/bin/perl
use FindBin;
use Getopt::Long;
use lib "$FindBin::Bin/../lib";
use v5.20;
use EQ2QR;

my @order;
my @orderEnd;
my $renderFile;
my $headerID;
my $headerName;
my $terminologyHeader = 'Terminology';
my $descriptionHeader = 'Description';
my $tableDescription;

GetOptions(
    'render-file=s'        => \$renderFile,
    'header-id=s'          => \$headerID,
    'header-name=s'        => \$headerName,
    'terminology-header=s' => \$terminologyHeader,
    'table-description=s'  => \$tableDescription,
    'bootstrap-term=s'     => \@order,
    'bootstrap-term-end=s' => \@orderEnd,
) || die;

die("--render-file is required") if !defined($renderFile);
die("--header-id is required")   if !defined($headerID);
die("--header-name is required") if !defined($headerName);

my %terms = %{ loadFromJSON($renderFile) };

push(@order,
    sort { sortableEntry($a, \%terms) cmp sortableEntry($b, \%terms) }
      keys %terms);
push(@order, @orderEnd);

say sectionHeader($headerID, $headerName, $terminologyHeader,
    $descriptionHeader, $tableDescription);

my %seen;
foreach my $lookupTerm (@order)
{
    my ($term, $content, $duplicate) = autoEntryFromAny($lookupTerm, \%terms);
    die("Failed to look up $lookupTerm") if !defined $content;
    if ($seen{$term})
    {
        next;
    }
    my $class = '';
    if ($duplicate)
    {
        $class = ' class="duplicate"';
    }
    $seen{$term} = 1;
    say '<tr'
      . $class . '><td>'
      . $term
      . '</td><td>'
      . autoResolveAbbrevs($content)
      . '</td></tr>';
}
say sectionFooter();
