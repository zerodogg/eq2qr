SHELL=/bin/bash
MINIFY=$(shell if which gominify &>/dev/null; then echo "gominify";else echo "minify";fi)
YARN=$(shell if which yarnpkg &>/dev/null; then echo "yarnpkg"; else echo "yarn -s";fi)
ESLINT=$(shell if [ -d node_modules ]; then echo "$(YARN) run eslint";else echo "eslint";fi)
SASS=$(shell if which sassc &>/dev/null; then echo "sassc";else echo "sass";fi)
GIT_REVISION=$(shell git rev-parse --short=8 HEAD)
STYLE_GIT_REVISION=$(shell git log -1 --format=%h style.scss)
MAIN_JS_GIT_REVISION=$(shell git log -1 --format=%h js/mainpage.js)
NUMHELPER_JS_GIT_REVISION=$(shell git log -1 --format=%h js/numhelper.js)
GIT_DATE=$(shell TZ=CET git log -1 --format=%ci)
OUT_DIRECTORY=./public
JSON_RENDERER=perl templates/json-renderer.pl
ROLLUP=$(YARN) rollup --sourcemap --config rollup.config.js --environment NODE_ENV:production

public: build
build: devBuild robots compress ccs
devBuild: clean css static html js
.PHONY: public static flow-typed
static: static/icon.png
	cp static/* public/
html: prepare static/icon.png mainPageHTML numHelperHTML addRevisions metadata
js: prepare mainPageJS numHelperJS
robots:
	(echo "";curl 'https://raw.githubusercontent.com/ai-robots-txt/ai.robots.txt/refs/heads/main/robots.txt') >> public/robots.txt
addRevisions:
	find public -name '*.html' -print0 | xargs -0 perl -pi -E 's/\{\{\{YEAR\}\}\}/$(shell date '+%Y')/g;s/\{\{\{NUMHELPER_JS_GIT_REVISION\}\}\}/$(NUMHELPER_JS_GIT_REVISION)/g; s/\{\{\{MAIN_JS_GIT_REVISION\}\}\}/$(MAIN_JS_GIT_REVISION)/g; s/\{\{\{STYLE_GIT_REVISION\}\}\}/$(STYLE_GIT_REVISION)/g; s/\{\{\{REVISION\}\}\}/$(GIT_REVISION)/g; s/\{\{\{DATE\}\}\}/$(GIT_DATE)/g'

ccs:
	rm -rf eq2qr-ccs
	mkdir -p eq2qr-ccs
	cp -r *.json *.js *.lock *.md *.scss .*rc .*.sh .*.yml .*ignore .*config js lib templates parts static Makefile eq2qr-ccs/
	rm -f eq2qr-ccs/static/icon.png
	echo "$(GIT_REVISION) // $(GIT_DATE)" > eq2qr-ccs/GitRevAndDate
	tar -jcf public/ccs.tar.bz2 eq2qr-ccs/
	rm -rf eq2qr-ccs

metadata:
	perl templates/sitemapbuilder.pl > $(OUT_DIRECTORY)/sitemap.xml

mainPage: prepare mainPageHTML mainPageJS
mainPageHTML: prepare
	mkdir -p $(OUT_DIRECTORY)
	perl templates/header.pl --is-main-page > $(OUT_DIRECTORY)/index.html
	cat parts/websites.html >> $(OUT_DIRECTORY)/index.html
	cat parts/tools.html >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/other.json --header-id 'other' --header-name 'Direct links to useful resources' --terminology-header 'Resource' >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/terminology.json --header-id 'terminology' --header-name 'Terminology, systems and features' >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/terminology-tradeskills.json --header-id 'tradeterms' --header-name 'Tradeskill terminology, systems and features' >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/addons.json --header-id 'addons' --header-name 'Addons' --bootstrap-term 'DarqUI Unified' --bootstrap-term 'EQ2Maps' >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/returner.json --header-id "returner" --header-name "Returning player" --terminology-header "Concept" --table-description "Here are some concepts that should get you back up to speed." --bootstrap-term "Overseer" --bootstrap-term "Familiar" --bootstrap-term "Wardrobe" --bootstrap-term "Mount equipment" --bootstrap-term "Fast travel/quick access teleport" --bootstrap-term "Panda quests" --bootstrap-term "Event Slot" --bootstrap-term-end "DarqUI Unified" >> $(OUT_DIRECTORY)/index.html
	cat parts/youtubers.html >> $(OUT_DIRECTORY)/index.html
	perl templates/events-renderer.pl --render-file=parts/live-events.json --header-id="liveevents" --header-name="Live events" --table-description="Upcoming and ongoing time-limited in-game events." >> $(OUT_DIRECTORY)/index.html
	$(JSON_RENDERER) --render-file parts/expansions.json --header-id "expansions" --header-name "Expansions and adventure packs" --terminology-header "Pack" --table-description "This table contains all the EverQuest 2 expansions and adventure packs in release order, along with the most notable changes in the release. Note that features that are no longer relevant (like infusing) are omitted from the description.<div class=\"requires-js sm-filter-links\">Filter: <a href=\"#\" id=\"expacShowOnlyLevelInc\">Show only expansions with level increases</a></div>" >> $(OUT_DIRECTORY)/index.html
	cat templates/main-footer.html >> $(OUT_DIRECTORY)/index.html
	cat templates/global-footer.html >> $(OUT_DIRECTORY)/index.html
mainPageJS:
	$(ROLLUP) js/mainpage.js --file public/logic.js

numHelper: prepare numHelperHTML numHelperJS
numHelperHTML: prepare
	mkdir -p $(OUT_DIRECTORY)/number-helper
	perl templates/header.pl --subtitle "Number helper" --description "A tool to help you understand some of EverQuest II's very large numbers." > $(OUT_DIRECTORY)/number-helper/index.html
	cat templates/numhelper.html >> $(OUT_DIRECTORY)/number-helper/index.html
	cat templates/global-footer.html >>  $(OUT_DIRECTORY)/number-helper/index.html
numHelperJS:
	$(ROLLUP) js/numhelper.js --file public/number-helper.js

css: prepare fontello
	$(SASS) ./style.scss ./public/style.css
	cat fontello/css/fontello.css >> public/style.css
static/icon.png:
	wget https://pbs.twimg.com/profile_images/315448946/Everquest_2_256x256_Icon_by_uematsufreak_400x400.png -O static/icon.png || wget https://eq2reference.org/icon.png -O static/icon.png
yarn-install:
	[ -d node_modules ] || $(YARN) install
deps: yarn-install flow-typed
prepare: deps
	mkdir -p public static
flow-typed:
	[ -d flow-typed ] || $(YARN) flow-typed install
clean:
	rm -rf public
	rm -f *.zip
distclean: clean
	rm -f static/icon.png
	rm -f ./.fontello*
	rm -rf fontello
compress:
	for ftype in html css js svg; do\
		for file in $$(find public \( -name "*.$$ftype" \)); do \
			mv "$$file" "$$file.tmp";\
			cat "$$file.tmp"|$(MINIFY) --html-keep-document-tags --type "$$ftype" -o "$$file";\
			rm -f "$$file.tmp";\
		done; \
	done
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' -o -name '*.svg' -o -name '*.ttf' \) -print0 | xargs -0 gzip -9 -k
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' -o -name '*.txt' -o -name '*.xml' -o -name '*.svg' -o -name '*.ttf' \) -print0 | xargs -0 brotli -k
onChange:
	while :;do echo ""; make --no-print-directory devBuild;sleep 0.3s;inotifywait -qq -e close,move,create . js templates parts ;done
basictest: deps
	for f in js/*.js; do $(ESLINT) $$f || exit 1;done
	for f in */*.pl; do perl -c $$f || exit 1;done
	$(YARN) flow check
	perl tools/jsonlint.pl
test: basictest html
linkcheck:
	bash .linkcheck.sh

fontello: ./.fontello.zip prepare
	rm -rf fontello
	unzip -q -d fontello ./.fontello.zip
	cd fontello && mv -f fontello*/* .
	perl -pi -E 's{../font/}{}' fontello/css/*css
	cp fontello/font/* static/
./.fontello.zip:
	curl --silent --show-error --fail --output .fontello --form "config=@./fontello.json" https://fontello.com/
	curl --silent --show-error --fail --output .fontello.zip https://fontello.com/`cat .fontello`/get
