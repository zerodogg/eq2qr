#!/usr/bin/perl
use FindBin;
use lib "$FindBin::Bin/../lib";
use v5.20;
use EQ2QR;
use File::Basename qw(basename);

my $exit = 0;

foreach my $JSON (glob($FindBin::Bin . '/../parts/*.json'))
{
    my $file  = basename($JSON);
    my %terms = %{ loadFromJSON($file) };
    if ($file ne 'live-events.json')
    {
        foreach my $term (sort keys %terms)
        {
            if (sortableEntry(getContentFromKey($term, \%terms)) !~ /\.\s*$/)
            {
                say "$file: $term: does not end in '.'";
                $exit = 1;
            }
        }
    }
}
exit $exit;
