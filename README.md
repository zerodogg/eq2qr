# EverQuest II: A resource

This repository contains the source code for the website «EverQuest II: A
resource», whose aim it is to be a useful reference for new, current and
returning players of EverQuest II.

The website itself is available at
[https://eq2reference.org](https://eq2reference.org).
