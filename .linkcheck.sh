#!/bin/bash
ret=0
verbosity="$1"
function testURL () {
    [ "$verbosity" == "-v" ] && echo "Testing $URL..."
    URL="$1"
    for attempt in 1 2 3; do
        if wget -U 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0' -qq -O/dev/null "$URL" &>/dev/null; then
            if [ "$verbosity" == "-v" ]; then
                echo "$URL: success"
            fi
            return 0
        fi
        sleep 30s
    done
    echo "Failed to wget $URL"
    ret=1
}
for URL in $(perl -MIO::All -MMojo::DOM -E 'my $d = Mojo::DOM->new(scalar io("./public/index.html")->slurp); say $d->text; $d->find("a")->each(sub { my $a = shift; my $link = $a->attr("href"); $link =~ s{#.*}{}; next if $link !~ /\S/; say $link});'|grep http|sort -u|grep -v gitlab.com); do
    testURL "$URL"
done
if [ "$ret" == "0" ]; then
    echo "All links successfully validated"
fi
exit $ret
